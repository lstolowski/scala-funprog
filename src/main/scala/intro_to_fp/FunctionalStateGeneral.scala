package intro_to_fp

/**
  * vol 6. continuation -> implementations for general use
  */
object FunctionalStateGeneral {

  /**
    * 6.10
    */
  // TODO
  case class State[S,+A](run: S => (A,S)) {

    def unit[A](a: A): State[S, A] = s => (a, s)

    def map[S,A,B](a: S => (A,S))(f: A => B): S => (B,S) = flatMap(a)(i => unit(f(i)))

    def map2[A,B,C](ra: State[S, A], rb: State[S, B])(f: (A, B) => C): State[S, C] =
      flatMap(ra)(a => map(rb)(b => f(a, b)))


    def flatMap[A,B](f: State[S, A])(g: A => State[S, B]): State[S, B] = rnd => {
      val (a, r) = f(rnd)
      g(a)(r)
    }

    def sequence[A](fs: List[State[S, A]]): State[S, List[A]] =
      fs.foldRight(unit(Nil:List[A]))((f, acc) => map2(f, acc)(_ :: _))

    def map[A,B](s: State[S, A])(f: A => B): State[S, B] = flatMap(s)(i => unit(f(i)))

  }

  object State {

  }

}
