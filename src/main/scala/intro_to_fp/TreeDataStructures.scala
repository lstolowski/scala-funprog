package intro_to_fp

/**
  * par. 3.5
  */
object TreeDataStructures extends App {

  sealed trait Tree[+A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]


  object Tree {

    /**
      * 3.25
      */
    def size[A](t: Tree[A]): Int = t match {
      case Leaf(_) => 1
      case Branch(l, r) => 1 + size(l) + size(r)
    }

    /**
      * 3.26
      */
    def maximum(t: Tree[Int]): Int = t match {
        case Leaf(value) => value
        case Branch(l, r) => maximum(l).max(maximum(r))
    }
    def minimum(t: Tree[Int]): Int = t match {
      case Leaf(value) => value
      case Branch(l, r) => minimum(l).min(minimum(r))
    }

    /**
      * 3.27
      */
    def depth(t: Tree[Int]): Int = {
      def loop(t: Tree[Int], max: Int): Int = t match {
        case Leaf(_) => max
        case Branch(l, r) => loop(l, max+1).max(loop(r, max+1))
      }

      loop(t, 0)
    }

    /**
      * 3.28
      */
    def map[A,B](t: Tree[A])(f: A => B): Tree[B] = t match {
      case Leaf(value) => Leaf(f(value))
      case Branch(l, r) => Branch(map(l)(f), map(r)(f))
    }

    /**
      * 3.29
      */
    def fold[A,B](t: Tree[A])(f: A => B)(g: (B,B) => B): B = t match {
      case Leaf(v) => f(v)
      case Branch(l, r) => g(fold(l)(f)(g), fold(r)(f)(g))
    }

    def size2[A](t: Tree[A]): Int   = fold(t)(v => 1)(1 + _ + _)
    def maximum2(t: Tree[Int]): Int = fold(t)(v => v)(_ max _)
    def depth2(t: Tree[Int]): Int   = fold(t)(v => 0)((dep1, dep2) => 1 + (dep1 max dep2) )
    def map2[A,B](t: Tree[A])(f: A => B): Tree[B] = fold(t)(v => Leaf(f(v)): Tree[B])(Branch(_, _))

  }





  println("*** TESTS ***")


  val tx = Branch(   Branch( Branch(Leaf(1), Leaf(3)), Leaf(4) ),   Branch(Leaf(10), Branch(Leaf(12), Leaf(5)))  )
  val dx = Branch(Branch(Branch(Leaf(2), Branch(Branch(Leaf(2), Leaf(1)), Leaf(1))), Leaf(1)), Leaf(1))


  println(s"size is ${Tree.size(tx)}")
  println(s"maximum is ${Tree.maximum(tx)}")
  println(s"minimum is ${Tree.minimum(tx)}")
  println(s"depth is ${Tree.depth(dx)}")
  println(s"map of ${tx} is ${Tree.map(tx)(_ * 2)}")

  println(s"size2 is ${Tree.size2(tx)}")
  println(s"maximum2 is ${Tree.maximum2(tx)}")
  println(s"depth2 is ${Tree.depth2(dx)}")
  println(s"map2 of ${tx} is ${Tree.map2(tx)(_ * 2)}")

}
