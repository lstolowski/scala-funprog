package intro_to_fp

/**
  * TODO - powtórka tego rozdziału :(
  */
object FunctionalState extends App {


  type Rand[+A] = RNG => (A, RNG)

  trait RNG {
    def nextInt: (Int, RNG)
  }
  case class SimpleRNG(seed: Long) extends RNG {
    def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = SimpleRNG(newSeed)
      val n = (newSeed >>> 16).toInt
        (n, nextRNG)
    }
  }

  /**
    * 6.1
    */
  def nonNegativeInt(rng: RNG): (Int, RNG) = rng.nextInt match {
    case (i, r) if i < 0 => (-1 * i, r)
    case (i, r) => (i, r)
  }

  /**
    * 6.2
    */
  def double(rng: RNG): (Double, RNG) = {
    val (v, r) = nonNegativeInt(rng)
    (v / (Int.MaxValue.toDouble + 1), r)
  }

  /**
    * 6.3
    */
  def intDouble(rng: RNG): ((Int,Double), RNG) = {
    val (i, r1) = nonNegativeInt(rng)
    val (d, r2) = double(r1)

    ((i, d), r2)
  }
  def doubleInt(rng: RNG): ((Double,Int), RNG) = {
    val ((i, d), r) = intDouble(rng)
    ((d, i), r)
  }
  def double3(rng: RNG): ((Double,Double,Double), RNG) = {
    val (d1, r1) = double(rng)
    val (d2, r2) = double(r1)
    val (d3, r3) = double(r2)
    ((d1, d2, d3), r3)
  }
  def int(rng: RNG): (Int, RNG) = rng.nextInt

  /**
    * 6.4
    */
  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    if (count > 0) {
      val (i, r) = rng.nextInt
      val (i2, r2) = ints(count - 1)(r)
      (i :: i2, r2)
    } else (Nil, rng)
  }

  // combinators:

  def unit[A](a: A): Rand[A] = rng => (a, rng)

  def map[A,B](s: Rand[A])(f: A => B): Rand[B] = rng => {
    val (a, rng2) = s(rng)
    (f(a), rng2)
  }

  def nonNegativeEven: Rand[Int] = map(nonNegativeInt)(i => i - i % 2)

  /**
    * 6.5
    */
  def double2: Rand[Double]= map(nonNegativeInt)(v => v / (Int.MaxValue.toDouble + 1 ))

  /**
    * 6.6
    */
  def map2[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = rng => {
    val (a, rng1) = ra(rng)
    val (b, rng2) = rb(rng1)
    (f(a, b), rng2)
  }

  def both[A,B](ra: Rand[A], rb: Rand[B]): Rand[(A,B)] = map2(ra, rb)((_, _))

  val randIntDouble: Rand[(Int, Double)] = both(int, double)
  val randDoubleInt: Rand[(Double, Int)] = both(double, int)


  /**
    * 6.7 (hard)
    */
  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] =
    fs.foldRight(unit(Nil:List[A]))((f, acc) => map2(f, acc)(_ :: _))

  def _ints(count: Int): Rand[List[Int]] =
    sequence(List.fill(count)(int))

  /**
    * 6.8
    */
  def flatMap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] = rnd => {
    val (a, r) = f(rnd)
    g(a)(r)
  }

  // using flatMap
  def nonNegativeLessThan(n: Int): Rand[Int] = flatMap(nonNegativeInt)(i => {
      val mod = i % n
      if (i + (n - 1) - mod >= 0) unit(mod) else nonNegativeLessThan(n)
    }
  )

  /**
    * 6.9
    */
  def mapViaFlatMap[A,B](s: Rand[A])(f: A => B): Rand[B] = flatMap(s)(i => unit(f(i)))

  def map2ViaFlatMap[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    flatMap(ra)(a => map(rb)(b => f(a, b)))

  // //////////////////////////////////////////////////////////////
  println("*** TESTS ***")

  val rng = SimpleRNG(scala.util.Random.nextInt)
  println("nonNegativeInt: " + nonNegativeInt(rng))
  println("double: " + double(rng))
  println("doubleInt: " + doubleInt(rng))
  println("intDouble: " + intDouble(rng))
  println("double3: " + double3(rng))
  println("ints: " + ints(5)(rng))
  println("nonNegativeEven: " + nonNegativeEven(rng))
  println("double2: " + double2(rng))
  println("randIntDouble: " + randIntDouble(rng))
  println("randDoubleInt: " + randDoubleInt(rng))
  println("_ints: " + _ints(5)(rng))

}
