package intro_to_fp


import intro_to_fp.Laziness.Stream.{cons, empty, unfold}

import scala.annotation.tailrec

/**
  * Par 5
  */
object Laziness extends App {

  sealed trait Stream[+A] {
    /**
      * 5.1
      */
    def toList: List[A] = {
      @tailrec
      def loop(str: Stream[A], acc: List[A]): List[A] = str match {
        case Empty => acc
        case Cons(head, tail) => loop(tail(), acc ::: List(head()))  // or head() :: acc with reverse at the end
      }

      loop(this, Nil)
    }
    // alternative solution:
    def toListNonTailrec : List[A] = this match {
      case Empty => Nil
      case Cons(head, tail) => head() :: tail().toList
    }

    /**
      * 5.2
      */
    def takeNonTailrec(n: Int): Stream[A] = this match {
      case Empty => Empty
      case Cons(h, t) => if (n > 0) cons(h(), t().take(n-1)) else Empty
    }

    def takeTailRec(n: Int): Stream[A] = {
      @tailrec
      def loop(n: Int, str: Stream[A], acc: Stream[A]): Stream[A] = str match {
        case Empty => acc
        case Cons(h, t) => if (n > 0) loop(n-1, t(), cons(h(), acc) ) else acc
      }
      loop(n, this, Empty)  //fixME - reversed order
    }

    def take(n: Int): Stream[A] = this match {
      case Cons(h, t) if n > 1 => cons(h(), t().take(n - 1))
      case Cons(h, _) if n == 1 => cons(h(), empty)
      case _ => empty
    }

    def drop(n: Int): Stream[A] = this match {
      case Empty => Empty
      case Cons(h, t) => if (n > 0) t().drop(n-1) else cons(h(), t().drop(n-1))
    }

    // alternative - from answerkey
    def drop2(n: Int): Stream[A] = this match {
      case Cons(_, t) if n > 0 => t().drop(n - 1)
      case _ => this
    }

    /**
      * 5.3
      */
    def takeWhile(p: A => Boolean): Stream[A] = this match {
      case Cons(h, t) if (p(h())) => cons(h(), t().takeWhile(p))
      case _ => empty
    }

    def filterBy(p: A => Boolean): Stream[A] = this match {
      case Empty => Empty
      case Cons(h, t) => if (p(h())) cons(h(), t().filterBy(p)) else t().filterBy(p)
    }

    def exists(p: A => Boolean): Boolean = this match {
      case Cons(h, t) => p(h()) || t().exists(p)
      case _ => false
    }
    def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
      case Cons(h,t) => f(h(), t().foldRight(z)(f))
      case _ => z
    }


    /**
      * 5.4
      */
    def forAll(p: A => Boolean): Boolean = this match {
      case Cons(h, t) => (p(h())) && t().forAll(p)
      case _ => true
    }  //alternative with foldRight: foldRight(true)((a,b) => f(a) && b)

    /**
      * 5.5
      */
    def takeWhile2(p: A => Boolean): Stream[A] =
      foldRight(empty:Stream[A])((a, b) => if (p(a)) cons(a, b) else empty)

    /**
      * 5.6
      */
    def headOption: Option[A] = foldRight(None: Option[A])((h, _) =>   Option(h)   )

    /**
      * 5.7 (using foldRight)
      */
    def map[B](f: A => B): Stream[B] = foldRight(empty:Stream[B])((a, b) => cons(f(a), b))

    def filter(p: A => Boolean): Stream[A] =
      foldRight(empty:Stream[A])((a, b) => if (p(a)) cons(a, b) else b)

    def append[B >: A](o: => Stream[B]): Stream[B] = foldRight(o)((a, b) => cons(a, b))
    def flatMap[B](f: A => Stream[B]): Stream[B] = foldRight(empty:Stream[B])((a, b) => f(a).append(b))

    /**
      * 5.13
      */
    def map2[B](f: A => B): Stream[B] = unfold(this) {
        case Cons(hd,tl) => Some((f(hd()), tl()))
        case _ => None
      }

    def take2(n: Int): Stream[A] = unfold((this, n)) {
      case (Cons(hd, tl), c) => if (c > 0) Some(hd(), (tl(), c-1)) else None
      case _ => None
    }

    def takeWhile3(p: A => Boolean): Stream[A] = unfold(this) {
      case Cons(hd, tl) => if(p(hd())) Some(hd(), tl()) else None
      case _ => None
    }

    def zipWith[B,C](s2: Stream[B])(f: (A, B) => C): Stream[C] = unfold((this, s2)) {
      case (Cons(h1, t1), Cons(h2,t2)) => Some(f(h1(), h2()), (t1(), t2()))
      case _ => None
    }

    def zipAll[B](s2: Stream[B]): Stream[(Option[A],Option[B])] = ??? // TODO

    /**
      * 5.14 (hard)
      */
    def startsWith[A](s: Stream[A]): Boolean = ??? // TODO

    /**
      * 5.15
      */
    def tails: Stream[Stream[A]] = unfold(this) {
      case Cons(h, t) => Some(Cons(h,t), t())
      case Empty => None
    } append empty

    /**
      * 5.16 (hard)
      */
    def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] = ??? // TODO


  }
  case object Empty extends Stream[Nothing]
  case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

  object Stream {
    // "smart constructor" -> lowercased by convention, avoids recomputation of hd and tl (i.e. cache'ing)
    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }
    // smart constructor for empty
    def empty[A]: Stream[A] = Empty

    // apply function is invoked when Stream(1,2,3,4)
    def apply[A](as: A*): Stream[A] =
      if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

    /**
      * 5.8
      */
    def constant[A](a: A): Stream[A] = cons(a, constant(a))

    /**
      * 5.9
      */
    def from(n: Int): Stream[Int] = cons(n, from(n+1))

    /**
      * 5.10
      */
    def fibs: Stream[Int] = {

      def loop(first: Int, second: Int): Stream[Int] = cons(second, loop(second, first+second))

      cons(0, loop(0, 1))
    }

    /**
      * 5.11
      */
    def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
      case Some((curr, next)) => cons(curr, unfold(next)(f))
      case None => empty
    }

    /**
      * 5.12
      */
    def fibs2: Stream[Int] = unfold((0, 1))(a => Some(a._1, (a._2, a._1 + a._2)))
    def from2(n: Int): Stream[Int] = unfold(n)(a => Some(a, a + 1))
    def constant2[A](a: A): Stream[A] = unfold(a)(_ => Some(a, a))
    def ones2: Stream[Int] = unfold(1)(_ => Some(1, 1))

  }

  println("**** Tests ****")

  println(s"toList ${Stream(1,2,3,4).toList}")
  println(s"toListNonTailrec ${Stream(1,2,3,4).toListNonTailrec}")
  println(s"take 3 ${Stream(1,2,3,4).take(3).toList}")
  println(s"drop 2 ${Stream(1,2,3,4).drop(2).toList}")
  println(s"takeWhile even ${Stream(2,4,5,6,7,8,9).takeWhile(_ % 2 == 0).toList}")
  println(s"filterBy even ${Stream(1,2,3,4,5,6,7,8,9).filterBy(_ % 2 == 0).toList}")
  println(s"forAll ${Stream(1,2,3,4,5,-6,7,8,9).forAll(_ > 0)}")
  println(s"takeWhile2 ${Stream(1,2,3,4,5,6,7,8,9).takeWhile2(a => a < 5 || a > 6).toList  }")
  println(s"headOption 1 ${Stream(1).headOption.get}")
  println(s"headOption None ${Stream().headOption.getOrElse("None")}")
  println(s"filter ${Stream(1,2,3,4,5,6,7,8,9).filter(a => a < 5 || a > 6).toList  }")
  println(s"append ${Stream(1,2,3,4).append(Stream(5,6,7)).toList}")
  println(s"flatMap ${Stream(1,2,3,4).flatMap(a => cons(a, empty)).toList}")

  println(s"from ${Stream.from(3).take(5).toList}")
  println(s"fibs ${Stream.fibs.take(10).toList}")


  val unfoldStr = unfold(10){ a => if (a == 0) None else Some((a, a-1)) }
  println(s"unfold ${unfoldStr.take(10).toList}")

  println(s"ones2 ${Stream.ones2.take(10).toList}")
  println(s"constant2 ${Stream.constant2(2).take(10).toList}")
  println(s"from2 ${Stream.from2(3).take(5).toList}")
  println(s"fibs2 ${Stream.fibs2.take(15).toList}")
  println(s"take2 3 ${Stream(1,2,3,4).take2(3).toList}")
  println(s"takeWhile3 ${Stream(1,2,3,4,5,6,7,8,9).takeWhile3(a => a < 5 || a > 6).toList  }")
  println(s"tails: ${Stream(1,2,3,4,5,6,7,8,9).tails.toList.foreach(x => print(" " + x.toList))  }")

}
