
List(1,2,3,4).scanLeft("a")((a,b) => a + b)
List(1,2,3,4).foldLeft("a")((a,b) => a + b)

val o = Option(1)
o.getOrElse(0)

val ax:List[Int] = List(1,2,3)

ax ++ List(5)




// Lazyness aka non-strict function aka call-by-name
/**
  * an argument that’s passed is unevaluated to a function
  * will be evaluated once for each place it’s referenced
  * in the body of the function
  */

def if2[A](cond: Boolean, onTrue: =>A, onFalse: => A): A =
  if (cond) onTrue else onFalse

def a():String = {println("in"); "in"}
if2(false, a(), "aa"+"11")


// caching call-by-name parameter
// to prevent evaluate it every time it is used:

def maybeTwice2(b: Boolean, i: => Int) = {
  lazy val j = i // lazy keyword will be evaluated on 1'st usage and then cached
  if (b) j+j else 0
}

val ran = new scala.util.Random
ran.nextBoolean()
ran.nextInt

