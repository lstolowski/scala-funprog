package intro_to_fp

import scala.annotation.tailrec

/**
  * par. 3.1-3.4
  */
object ListDataStructures extends App {

  sealed trait List[+A]
  case object Nil extends List[Nothing]
  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  object List {
    def sum(ints: List[Int]): Int = ints match {
      case Nil => 0
      case Cons(x,xs) => x + sum(xs)
    }

    def product(ds: List[Double]): Double = ds match {
      case Nil => 1.0
      case Cons(0.0, _) => 0.0
      case Cons(x,xs) => x * product(xs)
    }

    def apply[A](as: A*): List[A] =
      if (as.isEmpty) Nil
      else Cons(as.head, apply(as.tail: _*))

    /**
      * Exc 3.2
      */
    def tail[A](xs:List[A]): List[A] = xs match {
      case Nil => Nil
      case Cons(_, t) => t
    }

    /**
      * Exc 3.3
      */
    def setHead[A](head: A, xs:List[A]): List[A] = xs match {
      case Nil => Nil
      case Cons(_, t) => Cons(head, t)
    }

    /**
      * Exc 3.4
      */
    def drop[A](l: List[A], n: Int): List[A] = {
      def loop(l: List[A], n: Int): List[A] = l match {
        case Nil => Nil
        case Cons(h, t) => if (n == 1) t else loop(t, n-1)
      }
      loop(l, n)
    }
    /**
      * Exc 3.5
      */
    def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
      case Nil => Nil
      case Cons(h, t) => if (f(h)) dropWhile(t, f) else Cons(h, t)

      /** alternative with 'pattern guard' */
      // case Cons(h,t) if f(h) => dropWhile(t, f)
      // case _ => l
    }
    def dropWhileCurried[A](l: List[A])(f: A => Boolean): List[A] = l match {  // ponizej wywołanie
      case Nil => Nil
      case Cons(h, t) => if (f(h)) dropWhile(t, f) else Cons(h, t)

    }

    def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
      case Nil => a2
      case Cons(h,t) => Cons(h, append(t, a2))
    }

    /**
      * Exc 3.6
      */
    def init[A](l: List[A]): List[A] = l match {
      case Nil => Nil
      case Cons(_, Nil) => Nil
      case Cons(h, t) => Cons(h, init(t))
    }


    /**
      * example for generalizing function
      */
    def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B = as match {
        case Nil => z
        case Cons(x, xs) => f(x, foldRight(xs, z)(f))
      }
    def sum2(ns: List[Int]) = foldRight(ns, 0)(_ + _)
    def product2(ns: List[Double]) = foldRight(ns, 1.0)(_ * _)
    /**
      * Exc 3.8
      * foldRight(List(1,2,3), Nil:List[Int])(Cons(_,_)) GIVES ORIGINAL LIST
      */

    /**
      * Exc 3.9
      */
    def length[A](as: List[A]): Int = foldRight(as, 0)((_, b) => b + 1)
    /**
      * Exc 3.10
      */
    @tailrec
    def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
      case Nil => z
      case Cons(head, tail) => foldLeft(tail, f(z, head))(f)
    }

    /**
      * Exc 3.11
      */
    def sumLeft(ns: List[Int]) = foldLeft(ns, 0)(_ + _)
    def productLeft(ns: List[Double]) = foldLeft(ns, 1.0)(_ * _)
    def lengthLeft[A](as: List[A]): Int = foldLeft(as, 0)((b, _) => b + 1)

    /**
      * Exc 3.12
      */
    def reverse[A](xs: List[A]): List[A] = foldLeft(xs, List[A]())((t, h) => Cons(h, t))

    /**
      * Exc 3.13
      */
    def foldLeft2[A,B](as: List[A], z: B)(f: (B, A) => B): B = ??? // TODO
//    as match {
//      case Nil => z
//      case Cons(head, tail) => foldRight(tail, f(head, z))((a, b) => foldLeft2(a, b)(f) )
//    }
    def foldRight2[A,B](as: List[A], z: B)(f: (A, B) => B): B = ??? // TODO

    /**
      * 3.14
      */
    def append2[A](a1: List[A], a2: List[A]): List[A] = foldLeft(reverse(a1), a2)( (xs, h) => Cons(h, xs) )  // reverse is a workaround for folding Left ;)
    def append3[A](a1: List[A], a2: List[A]): List[A] = foldRight(a1, a2)( (h, xs) => Cons(h, xs) )  // better, without reverse

    /**
      * 3.15
      */
    def concat[A](xs: List[List[A]]): List[A] = foldLeft(xs, Nil:List[A])((acc, h) => append(acc, h) )


    /**
      * 3.16
      */
    def addOne(xs: List[Int]): List[Int] = {
      def loop(xs: List[Int], acc: List[Int]): List[Int] = xs match {
        case Nil => acc
        case Cons(h, t) => Cons(h + 1, loop(t, acc))

      }

      loop(xs, Nil)
    }
    def add1(xs: List[Int]): List[Int] = foldRight(xs, Nil:List[Int])((h, t) => Cons(h+1, t))

    /**
      * 3.17
      */
    def elemsToStr(xs: List[Double]): List[String] = foldRight(xs, Nil:List[String])((h, t) => Cons(h.toString, t))
    def elemsToStr2(xs: List[Double]): List[String] = foldLeft(xs, Nil:List[String])((t, h) => Cons(h.toString, t))

    /**
      * 3.18
      */
    def map[A,B](as: List[A])(f: A => B): List[B] = foldRight(as, Nil:List[B])((h, t) => Cons(f(h), t))

    /**
      * 3.19
      */
    def filter[A](as: List[A])(f: A => Boolean): List[A] = foldRight(as, Nil:List[A])((h, t) => if(f(h)) Cons(h, t) else t )

    /**
      * 3.20
      */
    def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = foldRight(as, Nil:List[B])((h, t) => append(f(h), t))
                                                              // alternative: concat(map(as)(f))
    /**
      * 3.21
      */
    def filter2[A](as: List[A])(f: A => Boolean): List[A] = flatMap(as)(a => if(f(a)) List(a) else Nil)
    /**
      * 3.22
      */
    def zip(xs: List[Int], ys: List[Int]): List[Int] = {
      def loop(xs: List[Int], ys: List[Int], acc: List[Int]): List[Int] = xs match {
        case Nil => acc
        case Cons(h1, t1) => ys match {
          case Nil => acc
          case Cons(h2, t2) => loop(t1, t2, append(acc, List(h1+h2)))
        }
      }

      loop(xs, ys, Nil)
    }
    // alternative (answerkey)
    def addPairwise(a: List[Int], b: List[Int]): List[Int] = (a,b) match {  // matching pairs of Lists
      case (Nil, _) => Nil
      case (_, Nil) => Nil
      case (Cons(h1,t1), Cons(h2,t2)) => Cons(h1+h2, addPairwise(t1,t2))
    }

    /**
      * 3.23
      */
    def zipWith[A,B,C](xs: List[A], ys: List[B])(f: (A, B) => C): List[C] = (xs, ys) match {
      case (Nil, _) => Nil
      case (_, Nil) => Nil
      case (Cons(h1, t1), Cons(h2, t2)) => Cons(f(h1, h2), zipWith(t1, t2)(f))
    }

    /**
      * 3.24
      */
    def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = ??? // TODO


  }



  /**
    * Exc 3.1
    */
  val x = List(1,2,3,4,5) match {
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Nil => 42
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case Cons(h, t) => h + List.sum(t)
    case _ => 101
  } //result : 3

  println("*** Tests ***")

  println(s"match is $x")
  val ys = List(1,2,3,4,5)
  println(s"tail is ${List.tail(ys)}")
  println(s"setHead is ${List.setHead(10, ys)}")
  println(s"drop is ${List.drop(ys, 2)}")
  println(s"dropWhile is ${List.dropWhile(ys, (a: Int) => a < 4)}")
  println(s"dropWhile curried is ${List.dropWhileCurried(ys)(a => a < 4)}")  // Zaleta curying -> The main reason for grouping the arguments this
                                                                             //                   way is to assist with type inference
                                                                             //  i.e. when a function definition contains multiple argument groups,
                                                                             //  type information flows from left to right across these argument groups
                                                                             //  Zaleta 2: partial apply function
                                                                             // val f2 = f1(x)_
                                                                             // f2(n)
  println(s"init is ${List.init(ys)}")
  println(s"length is ${List.length(ys)}")
  println(s"foldLeft is ${List.foldLeft(ys, 0)((b, _) => b + 1)}")
  println(s"reverse is ${List.reverse(ys)}")
  println(s"reverse is ${List.drop(ys, 2)}")

  println(s"append by foldLeft is ${List.append2(ys, List(11,12,13,14,15))}")
  println(s"append by foldRight is ${List.append3(ys, List(11,12,13,14,15))}")

  println(s"concat is ${List.concat(List(List(1,2,3), List(4,5,6), List(7,8,9)))}")
  println(s"addOne is ${List.addOne(ys)}")
  println(s"add1 is ${List.addOne(ys)}")
  println(s"elemsToStr is ${List.elemsToStr(List(1.1, 1.2, 1.3))}")
  println(s"elemsToStr2 is ${List.elemsToStr2(List(1.1, 1.2, 1.3))}")

  println(s"map is ${List.map(ys)(_ * 3)}")
  println(s"filter is ${List.filter(ys)( _ % 2 == 0)}")
  println(s"filter2 is ${List.filter2(ys)( _ % 2 != 0)}")
  println(s"flatMap is ${List.flatMap(ys)( i => List(i, i))}")
  println(s"zip is ${List.zip(List(1,2,3), List(1,2))}")
  println(s"addPairwise is ${List.addPairwise(List(1,2,3), List(1,2))}")
  println(s"zipWith is ${List.zipWith(List(1,2,3), List(1,2,3))(_*_)}")
}
