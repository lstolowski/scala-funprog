package intro_to_fp

import scala.annotation.tailrec

/**
  * par. 2
  */
object HigherOrderFunctions extends App {


  /**
    * Exc 2.1
    */
  def fibonacci(n: Int): Int = {
    @tailrec
    def fib(first: Int, second: Int, count: Int): Int = {
      if (count != n) {
        fib(second, first+second, count+1)
      } else {
        second
      }

    }

    fib(0, 1, 2)
  }

  def formatResult(name: String, n: Int, f: Int => Int) = {
    val msg = "The %s of %d is %d."
    msg.format(name, n, f(n))
  }

  val n = 6
  println(s"${formatResult("Fibonacci", 6, fibonacci)}")

  /**
    * Exc 2.2
    */
  def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean ={

    def loop(n: Int, sorted: Boolean): Boolean = {
        if(!sorted || as.length == n) sorted
        else if (as.length > n+1) loop(n+1, ordered(as(n), as(n+1)) )
        else sorted
    }

    loop(0, true)
  }

  val arr = Array(1,9,0)
  println("isSorted: " + isSorted(arr, (a: Int, b: Int) => a < b))


  // example
  def partial1[A,B,C](a: A, f: (A,B) => C): B => C = b => f(a, b)

  /**
    * Exc 2.3
    */
  def curry[A,B,C](f: (A, B) => C): A => (B => C) = {
    a => b => f(a, b)
  }

  /**
    * Exc 2.4
    */
  def uncurry[A,B,C](f: A => B => C): (A, B) => C ={
    (a, b) => f(a)(b)
  }

  /**
    * Exc 2.5
    */
  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    a => f(g(a))
  }

  // ZASTOSOWANIE CURRYING

//
//  def f(a: Int) : Int => Double = b => b * Math.sin(a)
//  def g(a: Int) : Int => Int = b => 2 * a
////  def fAndG = f andThen g
//
//
//
//  println(f(2){
//
//    def fun(a: Double):Int = 5
//
//    fun(2)
//
//
//  })



}
