package intro_to_fp

/**
  * par. 4.
  *
  * PART 1 Option as an error handling object
  *
  * Subject: Avoid Exception handling by Option and Either
  *          Modifications using flatMap and map
  *
  */
object ErrorHandlingsOption extends App {

  sealed trait Option[+A] {
    def map[B](f: A => B): Option[B]
    def flatMap[B](f: A => Option[B]): Option[B]    // alternative impl:   { map(f) getOrElse None }
    def getOrElse[B >: A](default: => B): B
    def orElse[B >: A](ob: => Option[B]): Option[B]
    def filter(f: A => Boolean): Option[A]
  }

  /**
    * 4.1 Implement trait methods
    * // could be implemented in trait with pattern matching use
    */
  case object None extends Option[Nothing] {
    override def map[B](f: Nothing => B): Option[B]              = None
    override def flatMap[B](f: Nothing => Option[B]): Option[B]  = None
    override def getOrElse[B >: Nothing](default: => B): B         = default
    override def orElse[B >: Nothing](ob: => Option[B]): Option[B] = ob
    override def filter(f: Nothing => Boolean): Option[Nothing]  = None
  }

  case class Some[+A](get: A) extends Option[A] {
    override def map[B](f: A => B): Option[B] = Some(f(get))
    override def flatMap[B](f: A => Option[B]): Option[B] = f(get)
    override def getOrElse[B >: A](default: => B): B = get
    override def orElse[B >: A](ob: => Option[B]): Option[B] = this
    override def filter(f: A => Boolean): Option[A] = if(f(get)) this else None
  }

  /**
    * 4.1
    */
  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  /**
    * 4.2
    */
  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs).flatMap(
      m => mean(
        xs.map(x => math.pow(x - m, 2))
      )
    )
  println(s"variance is ${variance(List(1.0, 2.0, 3.0))}")

  def Try[A](a: => A): Option[A] =
    try Some(a)
    catch { case e: Exception => None }


  def lift[A,B](f: A => B): Option[A] => Option[B] = _ map f
  println(s"lift is ${lift((x:Int) => x*2)(Some(2))}")

  /**
    * 4.3  -> przekształcanie funkcji aby przyjmowały i zwracały Option
    */
  def map2[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = a.flatMap(ax => b.map(bx => f(ax, bx)))

  def map3[A,B,C,D](a: Option[A], b: Option[B], c: Option[C])(f: (A, B, C) => D): Option[D] =
    a.flatMap(ax => b.flatMap(bx => c.map(cx => f(ax, bx, cx))))

  def map1[A,B](a: Option[A])(f: A => B): Option[B] = a.map(ax => f(ax))

  // usage examples
  /** funkcja biblioteczna, nie-modyfikowalna, chcemy aby operowała Optionami */
  def insuranceRateQuote(age: Int, numberOfSpeedingTickets: Int): Double = ???
  /** uzycie map2 */
  def parseInsuranceRateQuote(age: String, numberOfSpeedingTickets: String): Option[Double] = {
    val optAge: Option[Int] = Try(age.toInt)
    val optTickets: Option[Int] = Try(numberOfSpeedingTickets.toInt)

    map2(optAge, optTickets)(insuranceRateQuote)  // użycie insuranceRateQuote(Int, String) z argumanetami typu Option i zwrócenie Option
  }

  // notes:
  /** map2 using for */
  def map2WithFor[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    for {
      aa <- a
      bb <- b
    } yield f(aa, bb)


  /**
    * 4.4
    */
  def sequence[A](ax: List[Option[A]]): Option[List[A]] = ax match {
    case Nil => Some(Nil)
    case opt :: tail => opt.flatMap( o =>  sequence(tail).map(o :: _) )
  }
  def sequence_alternative[A](ax: List[Option[A]]): Option[List[A]] =
    ax.foldRight[Option[List[A]]] (Some(Nil))((x, y) => map2(x,y)(_ :: _))   // hmmm... magia

  /**
    * 4.5
    */
  def traverseNonEfficient[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = sequence(a.map(ax => f(ax)))

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = a match {
    case Nil => Some(Nil)
    case head :: tail => map2(f(head), traverse(tail)(f))(_ :: _)
  }

  def sequence2[A](ax: List[Option[A]]): Option[List[A]] = traverse(ax)(a => a.flatMap( s => Some(s)))
  // original answer:
  def sequence3[A](ax: List[Option[A]]): Option[List[A]] = traverse(ax)(a => a)


  println(s"traverse ${traverse(List(1,2,3,4,5,6,7))(x => Some(x))}")
  println(s"traverse ${traverseNonEfficient(List(1,2,3,4,5,6,7))(x => Some(x))}")

  println(s"sequence ${sequence(List(Some(1), Some(2), Some(3)))}")
  println(s"sequence2 ${sequence2(List(Some(1), Some(2), Some(3)))}")
  println(s"sequence3 ${sequence3(List(Some(1), Some(2), Some(3)))}")

}
