package intro_to_fp


/**
  * par. 4.
  *
  * PART 2 Either as an error handling object
  *
  * Subject: Avoid Exception handling by Option and Either
  *          Modifications using flatMap and map
  *
  */
object ErrorHandlingsEither {

  /**
    * Either => use when important is what error occured not only the fact it occured
    *        => also used when there are two possible cases of something
    */
  sealed trait Either[+E, +A] {

    /**
      * 4.6
      */

    def map[B](f: A => B): Either[E, B] = this match {
      case Left(e) => Left(e)
      case Right(v) => Right(f(v))
    }

    def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this match {
      case Left(e) => Left(e)
      case Right(v) => f(v)
    }

    def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
      case Left(_) => b
      case Right(v) => Right(v)
    }

    def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = this.flatMap(a => b.map(bb => f(a, bb)))

    // alternative using for:
    def map2Alt[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = for {
      a <- this
      bb <- b
    } yield f(a, bb)


  }

  case class Left[+E](value: E) extends Either[E, Nothing]
  case class Right[+A](value: A) extends Either[Nothing, A]


  /**
    * 4.7
    */
  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = traverse(es)(a=>a)
  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
    case Nil => Right(Nil)
    case h :: t => f(h).map2(traverse(t)(f))(_ :: _)
  }

  /**
    * 4.8
    */
  case class Person(name: Name, age: Age)
  sealed class Name(val value: String)
  sealed class Age(val value: Int)

  def mkName(name: String): Either[String, Name] =
    if (name == "" || name == null) Left("Name is empty.")
    else Right(new Name(name))

  def mkAge(age: Int): Either[String, Age] =
    if (age < 0) Left("Age is out of range.")
    else Right(new Age(age))

  /**
    * 4.8 - solution
    */
  def mkPerson(name: String, age: Int): Either[String, Person] =
    mkName(name).map2(mkAge(age))(Person(_, _))

  /*
    There are a number of variations on `Option` and `Either`.
    If we want to accumulate multiple errors, a simple approach is a new data type that lets us keep a list of errors
    in the data constructor that represents failures:

    trait Partial[+A,+B]
    case class Errors[+A](get: Seq[A]) extends Partial[A,Nothing]
    case class Success[+B](get: B) extends Partial[Nothing,B]

    There is a type very similar to this called `Validation` in the Scalaz library.
    You can implement `map`, `map2`, `sequence`, and so on for this type in such a way that errors are accumulated
    when possible (`flatMap` is unable to accumulate errors--can you see why?).
    This idea can even be generalized further--we don't need to accumulate failing values into a list;
    we can accumulate values using any user-supplied binary function.

    It's also possible to use `Either[List[E],_]` directly to accumulate errors,
    using different implementations of helper functions like `map2` and `sequence`.
*/



}
